# PHP Laravel Framework  #



### Core Topics ###

# Controller
  - Basic Overview Controller
  - Single Action Controller
  - Resource Controller
  - Custom Controller

# Route Overview
  - Route with View
  - Route with Controller
  - Route with Controller & View
  - Route with Single Parameter
  - Route with Multiple Parameter
  - Route with Group
  - Route with HyperLinks	
