<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function mySelf(){
    	return 'I love Programming....';
    }

    public function yourSelf(){
    	return 'I hate Programming....';
    }
}
